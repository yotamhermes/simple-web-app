import React, { Component, Fragment } from 'react';
import axios from 'axios';

class Values extends Component {
    constructor(props) {
        super(props);
        this.state = {
            user: ''
        }
    }

    handleClick = () => {
        this.fetchValues();
    }

    fetchValues = () => {
        const url = process.env.REACT_APP_API_URL;

        axios.get(`${url}/users/current`)
        .then(res => this.setState({ user: res.data }));
    }
    
    render() {
        return (
            <Fragment>
                <button onClick={this.handleClick}>
                    fetch
                </button>
                {
                    this.state.user
                }
            </Fragment>
        );
    }
}

export default Values;

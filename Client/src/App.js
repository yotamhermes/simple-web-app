import React, { Component } from 'react';
import Values from './Values';
import './App.css';

class App extends Component {
  render() {
    return (
      <div className="App">
        <Values />
      </div>
    );
  }
}

export default App;
